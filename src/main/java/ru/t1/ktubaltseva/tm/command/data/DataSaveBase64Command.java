package ru.t1.ktubaltseva.tm.command.data;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataSaveBase64Command extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-base64";

    @NotNull
    private final String DESC = "Save data to base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws SaveDataException {
        System.out.println("[SAVE BASE64 DATA]");
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_BASE64);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);

            @Nullable final byte[] bytes = byteArrayOutputStream.toByteArray();
            @Nullable final String base64Str = new BASE64Encoder().encode(bytes);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(base64Str.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new SaveDataException();
        }
    }

}
