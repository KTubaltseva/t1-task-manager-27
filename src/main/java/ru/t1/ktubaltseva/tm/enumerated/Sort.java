package ru.t1.ktubaltseva.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.exception.field.SortIncorrectException;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public static Sort toSort(@Nullable final String value) throws SortIncorrectException {
        if (value == null || value.isEmpty()) throw new SortIncorrectException();
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        throw new SortIncorrectException();
    }

}
