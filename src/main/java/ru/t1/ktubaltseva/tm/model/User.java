package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @Nullable
    private String lastName;

    @NotNull
    private Role role = Role.USUAL;

    private boolean locked = false;

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += login;
        if (email != null)
            result += "\t" + email;
        result += "\t" + role;
        return result;
    }
}
