package ru.t1.ktubaltseva.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    private final Date created = new Date();

    @NotNull
    private List<User> users = new ArrayList<>();

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

}
