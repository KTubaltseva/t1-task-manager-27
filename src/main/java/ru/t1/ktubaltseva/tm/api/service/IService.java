package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.AbstractEntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model) throws AbstractEntityNotFoundException;

    @NotNull
    Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    M findOneById(@Nullable String id) throws IdEmptyException, EntityNotFoundException;

    @NotNull
    M findOneByIndex(@Nullable Integer index) throws IndexIncorrectException, EntityNotFoundException;

    void removeAll();

    @NotNull
    M removeOne(@Nullable M model) throws EntityNotFoundException, UserNotFoundException;

    @NotNull
    M removeById(@Nullable String id) throws IdEmptyException, EntityNotFoundException;

    @NotNull
    M removeByIndex(@Nullable Integer index) throws IndexIncorrectException, EntityNotFoundException;

}
